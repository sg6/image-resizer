const sharp = require('sharp');
const fs = require('fs');

let dir = process.argv[2];
if(dir.substr(-1) !== "/") {
    dir = dir + "/";
}

if (!fs.existsSync(dir)) {
    console.log('This directory does not exist: ' + dir);
    console.log('The application will exit now');
    process.exit();
}

const files = fs.readdirSync(dir);

if (!fs.existsSync(dir + "res/")){
    fs.mkdirSync(dir + "res/");
}

files.forEach((file, idx) => {
    sharp(dir + file)
    .resize(2000,1600)
    .withoutEnlargement(true)
    .max()
    .toFile(dir + 'res/' + file)
    .then(() => {
        if(idx === files.length -1) {
            console.log(`Finished! Go to ${dir}res/ to see the result`);
        }
    })
})