# Node Image Resizer

Using `sharp` to resize all images from one directory. The resized images will be saved into the root's `res` subdirectory.

## Usage

1. Clone the repo
1. Run `npm install` or `yarn install` 
1. Run `node index.js DIRECTORY` where DIRECTORY is the path where the images are stored.